Pod::Spec.new do |s|
  s.name             = "LLAKeyValueStore"
  s.version          = "1.0.1"
  s.summary          = "LLAKeyValueStore provides an easy and optionally secure way to persist data locally on your iOS device"
  s.description      = <<-DESC
                       LLAKeyValueStore is a simple tool for storing and retrieveing data from the UserDefaults and / or from the keychain. It provides a simple, readable, typesafe, swifty syntax and makes the everyday task of persisting simple information easy and delightful.

                       let score: Int = retrievePlayerScore()
                         // persist the score
                       KeyValueStore.default["score"].int = score
                         // persist a password (saved in the Keychain)
                       KeyValueStore.secure["password"].string = "<<secret>>"
                         [...]
                         // retrieve the score later
                       let retrievedScore = KeyValueStore.default["score"].int
                         // retrieve the password later
                       let retrievedPassword = KeyValueStore.secure["password"].string

                       DESC
  s.license          = 'MIT'
  s.author           = { "Lutz Lameyer" => "radebruch@gmail.com" }
  s.source           = { :git => "https://gitlab.com/radebruch/llakeyvaluestore.git", :tag => s.version.to_s }
  s.homepage         = 'https://gitlab.com/radebruch/llakeyvaluestore'

  s.platform      = :ios, '10.0'
  s.swift_version = '5'
  s.requires_arc  = true

  s.source_files  = 'Pod/Classes/**/*.swift'
  
  s.frameworks    = 'SystemConfiguration'
end
