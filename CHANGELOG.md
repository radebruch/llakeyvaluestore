### 1.0.1 Swift 5
Project was updated to Swift 5. Also, some compiler warnings that came up with Xcode 10.2 were checked and resolved.

---
### 1.0.0 Initial Release
LLaKeyValueStore was finally released. 