// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

public protocol KeyValueStoreValue {
    var string: String? { get set }
    var int: Int? { get set }
    var double: Double? { get set }
    var float: Float? { get set }
    var bool: Bool? { get set }
    var array: [Any]? { get set }
    var dictionary: [AnyHashable: Any]? { get set }
    var url: URL? { get set }
    var data: Data? { get set }
    var date: Date? { get set }
}

public protocol KeyValueStoreProtocol {
    subscript(_ key: String) -> KeyValueStoreValue { get set }
}

public struct KeyValueStore {
    public static var `default`: KeyValueStoreProtocol = { UserDefaultsKeyValueStore() }()
    public static var secure: KeyValueStoreProtocol = { KeychainKeyValueStore() }()
}
