// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

struct AnyValueImpl: KeyValueStoreValue {
    var store: KeyValueStoreProtocol
    var key: String
    var value: Any?

    var string: String? {
        get {
            return value as? String
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var int: Int? {
        get {
            return value as? Int
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var double: Double? {
        get {
            return value as? Double
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var float: Float? {
        get {
            return value as? Float
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var bool: Bool? {
        get {
            return value as? Bool
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var array: [Any]? {
        get {
            return value as? [Any]
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var dictionary: [AnyHashable: Any]? {
        get {
            return value as? [AnyHashable: Any]
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var url: URL? {
        get {
            guard let path = value as? String else { return nil }
            return URL(string: path)
        }
        set {
            let path = newValue?.absoluteString
            value = path
            store[key] = self
        }
    }

    var data: Data? {
        get {
            return value as? Data
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var date: Date? {
        get {
            guard let timestamp = value as? TimeInterval else { return nil }
            return Date(timeIntervalSinceReferenceDate: timestamp)
        }
        set {
            let timestamp = newValue?.timeIntervalSinceReferenceDate
            value = timestamp
            store[key] = self
        }
    }
}
