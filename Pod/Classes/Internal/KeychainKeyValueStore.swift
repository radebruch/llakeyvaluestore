// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

class KeychainKeyValueStore: KeyValueStoreProtocol {
    subscript(key: String) -> KeyValueStoreValue {
        get {
            return DataValueImpl(store: self, key: key, value: retrieveData(key: key))
        }
        set {
            if retrieveData(key: key) != nil {
                if let value = (newValue as? DataValueImpl)?.value {
                    // modify existing value
                    updateData(key: key, data: value)
                } else {
                    // delete existing value
                    removeData(key: key)
                }
            } else {
                // save new value
                guard let value = (newValue as? DataValueImpl)?.value else { return }
                saveData(key: key, data: value)
            }
        }
    }

    private func retrieveData(key: String) -> Data? {
        let qDict: [NSString: Any] = [
            KeychainKeyValueStore.kSecClassValue: KeychainKeyValueStore.kSecClassGenericPasswordValue,
            KeychainKeyValueStore.kSecAttrServiceValue: key,
            KeychainKeyValueStore.kSecAttrAccountValue: KeychainKeyValueStore.Account,
            KeychainKeyValueStore.kSecReturnDataValue: KeychainKeyValueStore.kCFBoolTrue,
            KeychainKeyValueStore.kSecMatchLimitValue: KeychainKeyValueStore.kSecMatchLimitOneValue,
        ]
        let keyChainQuery = NSMutableDictionary(dictionary: qDict)
        var dataTypeRef: AnyObject?

        let status = SecItemCopyMatching(keyChainQuery, &dataTypeRef)
        if #available(iOS 11.3, *) {
            if status != errSecSuccess, status != errSecItemNotFound {
                if let msg = SecCopyErrorMessageString(status, nil) {
                    print("Loading value from secure key-value store failed: \(msg)")
                }
            }
        }
        return dataTypeRef as? Data
    }

    private func saveData(key: String, data: Data) {
        let qDict: [NSString: Any] = [
            KeychainKeyValueStore.kSecClassValue: KeychainKeyValueStore.kSecClassGenericPasswordValue,
            KeychainKeyValueStore.kSecAttrServiceValue: key,
            KeychainKeyValueStore.kSecAttrAccountValue: KeychainKeyValueStore.Account,
            KeychainKeyValueStore.kSecValueDataValue: data,
        ]
        let status = SecItemAdd(qDict as CFDictionary, nil)
        if #available(iOS 11.3, *) {
            if status != errSecSuccess {
                if let msg = SecCopyErrorMessageString(status, nil) {
                    print("Writing to secure key-value store failed: \(msg)")
                }
            }
        }
    }

    private func removeData(key: String) {
        let qDict: [NSString: Any] = [
            KeychainKeyValueStore.kSecClassValue: KeychainKeyValueStore.kSecClassGenericPasswordValue,
            KeychainKeyValueStore.kSecAttrServiceValue: key,
            KeychainKeyValueStore.kSecAttrAccountValue: KeychainKeyValueStore.Account,
            KeychainKeyValueStore.kSecReturnDataValue: KeychainKeyValueStore.kCFBoolTrue,
        ]
        let status = SecItemDelete(qDict as CFDictionary)
        if #available(iOS 11.3, *) {
            if status != errSecSuccess {
                if let msg = SecCopyErrorMessageString(status, nil) {
                    print("Removing item from secure key-value store failed: \(msg)")
                }
            }
        }
    }

    private func updateData(key: String, data: Data) {
        // for security: remove and add instead of update
        removeData(key: key)
        saveData(key: key, data: data)
    }

    private static let Account = "LLAKeyValueStore"
    private static let kCFBoolTrue = kCFBooleanTrue!
    private static let kSecClassValue = NSString(format: kSecClass)
    private static let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
    private static let kSecValueDataValue = NSString(format: kSecValueData)
    private static let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
    private static let kSecAttrServiceValue = NSString(format: kSecAttrService)
    private static let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
    private static let kSecReturnDataValue = NSString(format: kSecReturnData)
    private static let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)
    private static let kSecMatchLimitAllValue = NSString(format: kSecMatchLimitAll)
}
