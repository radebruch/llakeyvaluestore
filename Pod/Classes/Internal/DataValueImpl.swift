// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

struct DataValueImpl: KeyValueStoreValue {
    var store: KeyValueStoreProtocol
    var key: String
    var value: Data?

    var string: String? {
        get {
            if let value = value {
                return String(data: value, encoding: .utf8)
            }
            return nil
        }
        set {
            if let newValue = newValue {
                data = newValue.data(using: .utf8)
            } else {
                data = nil
            }
        }
    }

    var int: Int? {
        get {
            return (string as NSString?)?.integerValue
        }
        set {
            if let newValue = newValue {
                string = String(newValue)
            } else {
                data = nil
            }
        }
    }

    var double: Double? {
        get {
            return (string as NSString?)?.doubleValue
        }
        set {
            if let newValue = newValue {
                string = String(newValue)
            } else {
                data = nil
            }
        }
    }

    var float: Float? {
        get {
            return (string as NSString?)?.floatValue
        }
        set {
            if let newValue = newValue {
                string = String(newValue)
            } else {
                data = nil
            }
        }
    }

    var bool: Bool? {
        get {
            return (string as NSString?)?.boolValue
        }
        set {
            if let newValue = newValue {
                string = String(newValue)
            } else {
                data = nil
            }
        }
    }

    var array: [Any]? {
        get {
            if let data = data {
                do {
                    if let obj = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) {
                        return obj as? [Any]
                    } else {
                        return nil
                    }
                } catch _ {
                    return nil
                }
            } else {
                return nil
            }
        }
        set {
            if let newValue = newValue {
                data = NSKeyedArchiver.archivedData(withRootObject: newValue)
                return
            } else {
                data = nil
            }
        }
    }

    var dictionary: [AnyHashable: Any]? {
        get {
            if let data = data {
                do {
                    if let obj = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) {
                        return obj as? [AnyHashable: Any]
                    } else {
                        return nil
                    }
                } catch _ {
                    return nil
                }
            } else {
                return nil
            }
        }
        set {
            if let newValue = newValue {
                data = NSKeyedArchiver.archivedData(withRootObject: newValue)
                return
            } else {
                data = nil
            }
        }
    }

    var url: URL? {
        get {
            guard let path = string else { return nil }
            return URL(string: path)
        }
        set {
            string = newValue?.absoluteString
        }
    }

    var data: Data? {
        get {
            return value
        }
        set {
            value = newValue
            store[key] = self
        }
    }

    var date: Date? {
        get {
            guard let timestamp = (string as NSString?)?.doubleValue else { return nil }
            return Date(timeIntervalSinceReferenceDate: timestamp)
        }
        set {
            if let timestamp = newValue?.timeIntervalSinceReferenceDate {
                string = String(timestamp)
            } else {
                data = nil
            }
        }
    }
}
