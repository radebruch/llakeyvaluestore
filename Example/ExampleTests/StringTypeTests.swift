// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

@testable import LLAKeyValueStore
import XCTest

class StringTypeTests: XCTestCase {
    override func setUp() {
        KeyValueStore.default["test_string_doesNotExist"].string = nil
        KeyValueStore.default["test_string_ascii"].string = "The quick brown fox jumps over the lazy dog."
        KeyValueStore.default["test_string_intl"].string = "äöüÄÖÜß€😎🥶☠️🤬"
        KeyValueStore.secure["test_string_doesNotExist"].string = nil
        KeyValueStore.secure["test_string_ascii"].string = "The quick brown fox jumps over the lazy dog."
        KeyValueStore.secure["test_string_intl"].string = "äöüÄÖÜß€😎🥶☠️🤬"
    }

    override func tearDown() {
        KeyValueStore.default["test_string_doesNotExist"].string = nil
        KeyValueStore.default["test_string_ascii"].string = nil
        KeyValueStore.default["test_string_intl"].string = nil
        KeyValueStore.secure["test_string_doesNotExist"].string = nil
        KeyValueStore.secure["test_string_ascii"].string = nil
        KeyValueStore.secure["test_string_intl"].string = nil
    }

    func testDefault() {
        let valNil = KeyValueStore.default["test_string_doesNotExist"].string
        XCTAssertNil(valNil)

        if let valAscii = KeyValueStore.default["test_string_ascii"].string {
            XCTAssertEqual(valAscii, "The quick brown fox jumps over the lazy dog.")
        } else {
            XCTFail("expected non-nil value")
        }

        if let valIntl = KeyValueStore.default["test_string_intl"].string {
            XCTAssertEqual(valIntl, "äöüÄÖÜß€😎🥶☠️🤬")
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.default["test_string_ascii"].string = "#"
        let valStr = KeyValueStore.default["test_string_ascii"].string
        XCTAssertEqual(valStr, "#")

        KeyValueStore.default["test_string_ascii"].string = nil
        XCTAssertNil(KeyValueStore.default["test_string_ascii"].string)
    }

    func testSecure() {
        let valNil = KeyValueStore.secure["test_string_doesNotExist"].string
        XCTAssertNil(valNil)

        if let valAscii = KeyValueStore.secure["test_string_ascii"].string {
            XCTAssertEqual(valAscii, "The quick brown fox jumps over the lazy dog.")
        } else {
            XCTFail("expected non-nil value")
        }

        if let valIntl = KeyValueStore.secure["test_string_intl"].string {
            XCTAssertEqual(valIntl, "äöüÄÖÜß€😎🥶☠️🤬")
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.secure["test_string_ascii"].string = "#"
        let valStr = KeyValueStore.secure["test_string_ascii"].string
        XCTAssertEqual(valStr, "#")

        KeyValueStore.secure["test_string_ascii"].string = nil
        XCTAssertNil(KeyValueStore.secure["test_string_ascii"].string)
    }
}
