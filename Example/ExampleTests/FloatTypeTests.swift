// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

@testable import LLAKeyValueStore
import XCTest

class FloatTypeTests: XCTestCase {
    override func setUp() {
        KeyValueStore.default["test_float_doesNotExist"].float = nil
        KeyValueStore.default["test_float_1"].float = 1.01
        KeyValueStore.default["test_float_77"].float = 77.07
        KeyValueStore.secure["test_float_doesNotExist"].float = nil
        KeyValueStore.secure["test_float_1"].float = 1.01
        KeyValueStore.secure["test_float_77"].float = 77.07
    }

    override func tearDown() {
        KeyValueStore.default["test_float_doesNotExist"].float = nil
        KeyValueStore.default["test_float_1"].float = nil
        KeyValueStore.default["test_float_77"].float = nil
        KeyValueStore.secure["test_float_doesNotExist"].float = nil
        KeyValueStore.secure["test_float_1"].float = nil
        KeyValueStore.secure["test_float_77"].float = nil
    }

    func testDefault() {
        let valNil = KeyValueStore.default["test_float_doesNotExist"].float
        XCTAssertNil(valNil)

        if let val1 = KeyValueStore.default["test_float_1"].float {
            XCTAssertEqual(val1, 1.01)
        } else {
            XCTFail("expected non-nil value")
        }

        if let val77 = KeyValueStore.default["test_float_77"].float {
            XCTAssertEqual(val77, 77.07)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.default["test_float_1"].float = 3.1415
        let valFloat = KeyValueStore.default["test_float_1"].float
        XCTAssertEqual(valFloat, 3.1415)

        KeyValueStore.default["test_float_1"].float = nil
        XCTAssertNil(KeyValueStore.default["test_float_1"].float)
    }

    func testSecure() {
        let valNil = KeyValueStore.secure["test_float_doesNotExist"].float
        XCTAssertNil(valNil)

        if let val1 = KeyValueStore.secure["test_float_1"].float {
            XCTAssertEqual(val1, 1.01)
        } else {
            XCTFail("expected non-nil value")
        }

        if let val77 = KeyValueStore.secure["test_float_77"].float {
            XCTAssertEqual(val77, 77.07)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.secure["test_float_1"].float = 3.1415
        let valFloat = KeyValueStore.secure["test_float_1"].float
        XCTAssertEqual(valFloat, 3.1415)

        KeyValueStore.secure["test_float_1"].float = nil
        XCTAssertNil(KeyValueStore.secure["test_float_1"].float)
    }
}
