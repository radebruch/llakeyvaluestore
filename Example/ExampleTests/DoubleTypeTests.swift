// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

@testable import LLAKeyValueStore
import XCTest

class DoubleTypeTests: XCTestCase {
    override func setUp() {
        KeyValueStore.default["test_double_doesNotExist"].double = nil
        KeyValueStore.default["test_double_1"].double = 1.01
        KeyValueStore.default["test_double_77"].double = 77.07
        KeyValueStore.secure["test_double_doesNotExist"].double = nil
        KeyValueStore.secure["test_double_1"].double = 1.01
        KeyValueStore.secure["test_double_77"].double = 77.07
    }

    override func tearDown() {
        KeyValueStore.default["test_double_doesNotExist"].double = nil
        KeyValueStore.default["test_double_1"].double = nil
        KeyValueStore.default["test_double_77"].double = nil
        KeyValueStore.secure["test_double_doesNotExist"].double = nil
        KeyValueStore.secure["test_double_1"].double = nil
        KeyValueStore.secure["test_double_77"].double = nil
    }

    func testDefault() {
        let valNil = KeyValueStore.default["test_double_doesNotExist"].double
        XCTAssertNil(valNil)

        if let val1 = KeyValueStore.default["test_double_1"].double {
            XCTAssertEqual(val1, 1.01)
        } else {
            XCTFail("expected non-nil value")
        }

        if let val77 = KeyValueStore.default["test_double_77"].double {
            XCTAssertEqual(val77, 77.07)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.default["test_double_1"].double = 3.1415
        let valDouble = KeyValueStore.default["test_double_1"].double
        XCTAssertEqual(valDouble, 3.1415)

        KeyValueStore.default["test_double_1"].double = nil
        XCTAssertNil(KeyValueStore.default["test_double_1"].double)
    }

    func testSecure() {
        let valNil = KeyValueStore.secure["test_double_doesNotExist"].double
        XCTAssertNil(valNil)

        if let val1 = KeyValueStore.secure["test_double_1"].double {
            XCTAssertEqual(val1, 1.01)
        } else {
            XCTFail("expected non-nil value")
        }

        if let val77 = KeyValueStore.secure["test_double_77"].double {
            XCTAssertEqual(val77, 77.07)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.secure["test_double_1"].double = 3.1415
        let valDouble = KeyValueStore.secure["test_double_1"].double
        XCTAssertEqual(valDouble, 3.1415)

        KeyValueStore.secure["test_double_1"].double = nil
        XCTAssertNil(KeyValueStore.secure["test_double_1"].double)
    }
}
