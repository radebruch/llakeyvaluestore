// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

@testable import LLAKeyValueStore
import XCTest

class DictionaryTypeTests: XCTestCase {
    override func setUp() {
        KeyValueStore.default["test_dictionary_doesNotExist"].dictionary = nil
        KeyValueStore.default["test_dictionary_1"].dictionary = ["One": 1, "Two": "Zwo", "Three": ["1", 2, 3.0]]
        KeyValueStore.secure["test_dictionary_doesNotExist"].dictionary = nil
        KeyValueStore.secure["test_dictionary_1"].dictionary = ["One": 1, "Two": "Zwo", "Three": ["1", 2, 3.0]]
    }

    override func tearDown() {
        KeyValueStore.default["test_dictionary_doesNotExist"].dictionary = nil
        KeyValueStore.default["test_dictionary_1"].dictionary = nil
        KeyValueStore.secure["test_dictionary_doesNotExist"].dictionary = nil
        KeyValueStore.secure["test_dictionary_1"].dictionary = nil
    }

    func testDefault() {
        let valNil = KeyValueStore.default["test_dictionary_doesNotExist"].dictionary
        XCTAssertNil(valNil)

        if let valDict = KeyValueStore.default["test_dictionary_1"].dictionary {
            XCTAssertEqual(valDict.count, 3)
            XCTAssertEqual(valDict["One"] as? Int, 1)
            XCTAssertEqual(valDict["Two"] as? String, "Zwo")
            XCTAssertEqual((valDict["Three"] as? [Any])?.count, 3)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.default["test_dictionary_1"].dictionary = ["Horst": "Jörg"]
        let valDict = KeyValueStore.default["test_dictionary_1"].dictionary
        XCTAssertEqual(valDict as? Dictionary, ["Horst": "Jörg"])

        KeyValueStore.default["test_dictionary_1"].dictionary = nil
        XCTAssertNil(KeyValueStore.default["test_dictionary_1"].dictionary)
    }

    func testSecure() {
        let valNil = KeyValueStore.secure["test_dictionary_doesNotExist"].dictionary
        XCTAssertNil(valNil)

        if let valDict = KeyValueStore.secure["test_dictionary_1"].dictionary {
            XCTAssertEqual(valDict.count, 3)
            XCTAssertEqual(valDict["One"] as? Int, 1)
            XCTAssertEqual(valDict["Two"] as? String, "Zwo")
            XCTAssertEqual((valDict["Three"] as? [Any])?.count, 3)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.secure["test_dictionary_1"].dictionary = ["Horst": "Jörg"]
        let valDict = KeyValueStore.secure["test_dictionary_1"].dictionary
        XCTAssertEqual(valDict as? Dictionary, ["Horst": "Jörg"])

        KeyValueStore.secure["test_dictionary_1"].dictionary = nil
        XCTAssertNil(KeyValueStore.secure["test_dictionary_1"].dictionary)
    }
}
