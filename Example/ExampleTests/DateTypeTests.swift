// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

@testable import LLAKeyValueStore
import XCTest

class DateTypeTests: XCTestCase {
    override func setUp() {
        KeyValueStore.default["test_date_doesNotExist"].date = nil
        KeyValueStore.default["test_date_1"].date = Date(timeIntervalSince1970: 32_700_068.69)
        KeyValueStore.secure["test_date_doesNotExist"].date = nil
        KeyValueStore.secure["test_date_1"].date = Date(timeIntervalSince1970: 32_700_068.69)
    }

    override func tearDown() {
        KeyValueStore.default["test_date_doesNotExist"].date = nil
        KeyValueStore.default["test_date_1"].date = nil
        KeyValueStore.secure["test_date_doesNotExist"].date = nil
        KeyValueStore.secure["test_date_1"].date = nil
    }

    func testDefault() {
        let valNil = KeyValueStore.default["test_date_doesNotExist"].date
        XCTAssertNil(valNil)

        if let valDate = KeyValueStore.default["test_date_1"].date {
            XCTAssertEqual(valDate, Date(timeIntervalSince1970: 32_700_068.69))
        } else {
            XCTFail("expected non-nil value")
        }

        let theDate = Date()
        KeyValueStore.default["test_date_1"].date = theDate
        let valDate = KeyValueStore.default["test_date_1"].date
        XCTAssertNotNil(valDate)
        XCTAssertEqual(valDate, theDate)

        KeyValueStore.default["test_date_1"].date = nil
        XCTAssertNil(KeyValueStore.default["test_date_1"].date)
    }

    func testSecure() {
        let valNil = KeyValueStore.secure["test_date_doesNotExist"].date
        XCTAssertNil(valNil)

        if let valDate = KeyValueStore.secure["test_date_1"].date {
            XCTAssertEqual(valDate, Date(timeIntervalSince1970: 32_700_068.69))
        } else {
            XCTFail("expected non-nil value")
        }

        let theDate = Date()
        KeyValueStore.secure["test_date_1"].date = theDate
        let valDate = KeyValueStore.secure["test_date_1"].date
        XCTAssertNotNil(valDate)
        XCTAssertEqual(valDate, theDate)

        KeyValueStore.secure["test_date_1"].date = nil
        XCTAssertNil(KeyValueStore.secure["test_date_1"].date)
    }
}
