// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

@testable import LLAKeyValueStore
import XCTest

class BoolTypeTests: XCTestCase {
    override func setUp() {
        KeyValueStore.default["test_bool_doesNotExist"].bool = nil
        KeyValueStore.default["test_bool_true"].bool = true
        KeyValueStore.default["test_bool_false"].bool = false
        KeyValueStore.secure["test_bool_doesNotExist"].bool = nil
        KeyValueStore.secure["test_bool_true"].bool = true
        KeyValueStore.secure["test_bool_false"].bool = false
    }

    override func tearDown() {
        KeyValueStore.default["test_bool_doesNotExist"].bool = nil
        KeyValueStore.default["test_bool_true"].bool = nil
        KeyValueStore.default["test_bool_false"].bool = nil
        KeyValueStore.secure["test_bool_doesNotExist"].bool = nil
        KeyValueStore.secure["test_bool_true"].bool = nil
        KeyValueStore.secure["test_bool_false"].bool = nil
    }

    func testDefault() {
        let valNil = KeyValueStore.default["test_bool_doesNotExist"].bool
        XCTAssertNil(valNil)

        if let valTrue = KeyValueStore.default["test_bool_true"].bool {
            XCTAssertTrue(valTrue)
        } else {
            XCTFail("expected non-nil value")
        }

        if let valFalse = KeyValueStore.default["test_bool_false"].bool {
            XCTAssertFalse(valFalse)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.default["test_bool_true"].bool = false
        let valBool = KeyValueStore.default["test_bool_true"].bool
        XCTAssertNotNil(valBool)
        XCTAssertEqual(valBool, false)

        KeyValueStore.default["test_bool_true"].bool = nil
        XCTAssertNil(KeyValueStore.default["test_bool_true"].bool)
    }

    func testSecure() {
        let valNil = KeyValueStore.secure["test_bool_doesNotExist"].bool
        XCTAssertNil(valNil)

        if let valTrue = KeyValueStore.secure["test_bool_true"].bool {
            XCTAssertTrue(valTrue)
        } else {
            XCTFail("expected non-nil value")
        }

        if let valFalse = KeyValueStore.secure["test_bool_false"].bool {
            XCTAssertFalse(valFalse)
        } else {
            XCTFail("expected non-nil value")
        }

        KeyValueStore.secure["test_bool_true"].bool = false
        let valBool = KeyValueStore.secure["test_bool_true"].bool
        XCTAssertNotNil(valBool)
        XCTAssertEqual(valBool, false)

        KeyValueStore.secure["test_bool_true"].bool = nil
        XCTAssertNil(KeyValueStore.secure["test_bool_true"].bool)
    }
}
