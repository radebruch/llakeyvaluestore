// MIT License
//
// Copyright © 2017-2019 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import LLAKeyValueStore
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // The following code doesn't do anything sensible. It just demonstrates, how the
        // KeyValueStore can be used.

        let earnings = [
            "Anna": 17350.00,
            "Mike": 11220.50,
            "Dana": 6333.88,
            "Jack": 14498.20,
        ]
        // save the earnings in the secure store (i.e. goes to the keychain)
        KeyValueStore.secure["earnings_key"].dictionary = earnings

        // Remember that we have saved the earnings (this goes to the UserDefaults):
        KeyValueStore.default["earnings_saved_key"].bool = true

        // now retrieve the values:
        // Note how you must cast the dictionary - the type information is lost here
        if KeyValueStore.default["earnings_saved_key"].bool == true {
            print("Earnings were saved")
            let retrievedDictionary = KeyValueStore.secure["earnings_key"].dictionary as? [String: Double]
            print(retrievedDictionary ?? "nil")
        } else {
            print("oops")
        }

        // Delete the values just created / added:
        KeyValueStore.secure["earnings_key"].dictionary = nil
        KeyValueStore.default["earnings_saved_key"].bool = nil

        // In your test setup, call mock() to detach the persistent store and switch to a temporary
        // volatile storage method. This way you make sure you don't interfere with actual data
        KeyValueStore.mock()

        // Now insert your test data
        KeyValueStore.default["userName"].string = "John Doe"
        KeyValueStore.secure["password"].string = "youneverguess"
        KeyValueStore.secure["refreshToken"].data = Data([11, 12, 13, 14])
        KeyValueStore.default["score"].int = 777
        KeyValueStore.default["homepage"].url = URL(string: "https://www.google.com")
        KeyValueStore.secure["trialExpiration"].date = Date(timeIntervalSinceNow: 30 * 24 * 60 * 60)

        //
        // [Then make your tests]
        //

        // and finally call
        KeyValueStore.stopMocking()

        // and now the original persistent stores are re-attached

        return true
    }
}
