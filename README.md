# Simple Secure Storage

there's always need to persistently store and later retrieve some simple values. Sometimes it's the score of a player, sometimes it's a simple flag indicating that the user has already seen the tutorial.
And on iOS there's an ideal place to store these values: `UserDefaults`.

But the `UserDefaults` are not a secure place to store more critical information like passwords or
access tokens. You need a more secure place to store these data.

`LLAKeyValueStore` provides a simple, delightful, swifty and type-safe API to store such information. Let's start with the normal case:

```swift
    // Store an integer
    KeyValueStore.default["score_key"].int = 3345
    // [...]
    // later, retrieve the integer:
    let retrievedScore = KeyValueStore.default["score_key"].int
```

It's as simple as that. The supported types that you can save directly are: `String`, `Int`, `Double`, `Float`, `Bool`, `Data`, `Date`, `URL` and, with some limitations, `Array` and `Dictionary`.

And here's how you store a password securely:

```swift
    // Securely store a password
    KeyValueStore.secure["password_key"].string = "secret"
    // [...]
    // later, retrieve the password:
    let retrievedPassword = KeyValueStore.secure["password_key"].string
```
That's right. It's just exactly the same pattern. Note, however, that the `secure` store is used this time, while in the first example, it was the `default` store.

# `secure` vs. `default`

There's no difference in the way the stores are used. What makes the difference is where they store the data: the `default` store is backed by `UserDefaults`, while the `secure` store is backed by the key chain.

# Array and Dictionary

As mentioned above, there's a limitation when it comes to storing arrays and dictionaries. These types can only be stored successfully, if they contain data that can be encoded by `NSKeyedArchiver`. This is the case for all number types, bools, strings, instances of Data. Other types like tupels, enums, functions or cannot be included. Arrays and dictionaries that conform to this restriction can be nested. 
The reason for this restriction is, that the arrays and dictionaries must be serialized in order to be stored. Internally, this is accomplished by using a `NSKeyedArchiver` which has its limitations when it comes to swift specific types.

# Support for your tests

There's an extension to `KeyValueStore` that adds the two static methods `mock()` and `stopMocking()`. If you call `mock()` the persistent storage will be disconnected and internally switched to a simple temporary store. This can be reversed by calling `stopMocking()`, which re-attaches the persistent stores.

This is intended to help you writing your tests. If want to test some code that uses `KeyValueStore` you can call `mock()` during your test setup and then set arbitrary values for the test without interfering with the actual persistent store.

# Installation

Via [Cocoapods](https://guides.cocoapods.org/using/index.html). 
Add the following line to your Podfile:

```ruby
    pod 'LLAKeyValueStore'
```

Then, whenever you want to use the store, you need to

```swift
    import LLAKeyValueStore
````

# License

`LLAKeyValueStore` is distributed under the terms of the [MIT License](https://opensource.org/licenses/MIT) (see also LICENSE file in this directory)
