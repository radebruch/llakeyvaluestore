task :default => :format

#------------------------------------------------------------------------------------------------------------------------------------
desc "Format the Swift code in the project"
task :format do
    locations = " './Pod' './Example/Example' './Example/ExampleTests'"
    system "swiftformat --config .swiftformat" + locations
end

#------------------------------------------------------------------------------------------------------------------------------------
desc "Run the Realm SwiftLint tool"
task :lint do
    system "swiftlint"
end

#------------------------------------------------------------------------------------------------------------------------------------
desc "Manually run format & lint"
task :precommit => [:format, :lint] do
end
